---
title: Portfolio
description: The portfolio of Jonathan Franklin
draft: false
landing:
  height: 500
  image: favicon/android-icon-192x192.png
  title:
    - Home
  text:
    - This is Hugo Z Themes documentation site
  titleColor:
  textColor:		
  spaceBetweenTitleText: 25
  buttons:
    - link: docs
      text: HUGO THEME ZDOC
      color: primary
  # backgroundImage: 
  #   src: images/landscape.jpg
  #   height: 600
footer:
#  sections:
#    - title: General
#      links:
#        - title: Docs
#          link: https://gohugo.io/
#        - title: Learn
#         link: https://gohugo.io/
#        - title: Showcase
#          link: https://gohugo.io/
#        - title: Blog
#          link: https://gohugo.io/
#    - title: resources
#     links:
#        - title: GitHub
#          link: https://gohugo.io/
#        - title: Releases
#         link: https://gohugo.io/
#        - title: Spectrum
#          link: https://gohugo.io/
#       - title: Telemetry
#          link: https://gohugo.io/
#   - title: Features
#      links:
#        - title: GitHub
#          link: https://gohugo.io/
#        - title: Releases
#          link: https://gohugo.io/
#        - title: Spectrum
#          link: https://gohugo.io/
#        - title: Telemetry
#          link: https://gohugo.io/
  contents: 
    align: left
    applySinglePageCss: false
    markdown:
      |
      ## Cnidaria
      Copyright © 2022. All rights reserved.
---